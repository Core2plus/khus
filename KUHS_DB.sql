/*
SQLyog Ultimate v12.2.6 (64 bit)
MySQL - 10.1.25-MariaDB : Database - kuhs_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`kuhs_db` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `kuhs_db`;

/*Table structure for table `allotmentorder` */

DROP TABLE IF EXISTS `allotmentorder`;

CREATE TABLE `allotmentorder` (
  `Allotmentid` int(5) NOT NULL,
  `AllotmentNo` int(30) NOT NULL,
  `AllotmentDate` date NOT NULL,
  `MemberID` int(5) NOT NULL,
  `MembershipNo` varchar(20) DEFAULT NULL,
  `MemberName` varchar(100) DEFAULT NULL,
  `FatherName` varchar(100) DEFAULT NULL,
  `NICN` varchar(20) DEFAULT NULL,
  `BlockNo` varchar(3) DEFAULT NULL,
  `Category` varchar(3) DEFAULT NULL,
  `PlotNo` varchar(10) DEFAULT NULL,
  `AreaSqYard` varchar(10) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  KEY `MemIdFK` (`MemberID`),
  CONSTRAINT `MemIdFK` FOREIGN KEY (`MemberID`) REFERENCES `memberprofile` (`Memberid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `allotmentorder` */

/*Table structure for table `chargestype` */

DROP TABLE IF EXISTS `chargestype`;

CREATE TABLE `chargestype` (
  `ChargesID` int(5) NOT NULL AUTO_INCREMENT,
  `ChargesCode` varchar(10) NOT NULL,
  `ChargesDescription` varchar(100) DEFAULT NULL,
  `Charges` decimal(10,0) NOT NULL,
  PRIMARY KEY (`ChargesID`,`ChargesCode`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `chargestype` */

insert  into `chargestype`(`ChargesID`,`ChargesCode`,`ChargesDescription`,`Charges`) values 
(1,'001','Admission Fees Rs.',5),
(2,'002','Share @ Rs. As Per Share',25),
(3,'003','Cost Of Land..................Installment',0),
(4,'004','Ext. Development Charges......Installment',1000),
(5,'005','Int. Development Charges......Installment',0),
(6,'006','Lease Charges ...........................',0),
(7,'007','Other Charges ...........................',0),
(8,'008','Mis.  Charges ...........................',0),
(9,'009','Electric Charges ........................',0),
(10,'010','Late Charges ............................',0);

/*Table structure for table `createplotfiles` */

DROP TABLE IF EXISTS `createplotfiles`;

CREATE TABLE `createplotfiles` (
  `FileID` int(5) NOT NULL AUTO_INCREMENT,
  `FileNo` varchar(20) NOT NULL,
  `MemberID` int(5) NOT NULL,
  `MembershipNo` varchar(20) NOT NULL,
  `MemberName` varchar(100) DEFAULT NULL,
  `SocietyId` int(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`FileID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `createplotfiles` */

/*Table structure for table `employee` */

DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `prof_img` text,
  `date_of_join` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `employee` */

/*Table structure for table `expensetype` */

DROP TABLE IF EXISTS `expensetype`;

CREATE TABLE `expensetype` (
  `ExpenseID` int(5) NOT NULL AUTO_INCREMENT,
  `ExpenseCode` varchar(10) NOT NULL,
  `ExpenseNature` varchar(100) DEFAULT NULL,
  `Expense` decimal(10,0) NOT NULL,
  PRIMARY KEY (`ExpenseID`,`ExpenseCode`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `expensetype` */

insert  into `expensetype`(`ExpenseID`,`ExpenseCode`,`ExpenseNature`,`Expense`) values 
(1,'001','Transportation',500),
(2,'002','Petrol',25),
(3,'003','Maintenance',300),
(4,'004','Lunch',250),
(5,'005','Dinner',300),
(6,'006','Dinner Party',5000);

/*Table structure for table `filetransfer` */

DROP TABLE IF EXISTS `filetransfer`;

CREATE TABLE `filetransfer` (
  `FT_ID` int(5) NOT NULL AUTO_INCREMENT,
  `FileNo` varchar(20) NOT NULL,
  `Allotmentid` int(5) NOT NULL,
  `PlotInfo` varchar(100) DEFAULT NULL,
  `Owner_MemID` int(5) NOT NULL,
  `Owner_NICN` varchar(20) DEFAULT NULL,
  `Buyer_MemID` int(5) DEFAULT NULL,
  `Buyer_NICN` varchar(20) DEFAULT NULL,
  `FileTransferFees` decimal(20,0) DEFAULT NULL,
  `FTDate` date DEFAULT NULL,
  PRIMARY KEY (`FT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `filetransfer` */

/*Table structure for table `filetransferhistory` */

DROP TABLE IF EXISTS `filetransferhistory`;

CREATE TABLE `filetransferhistory` (
  `FT_ID` int(5) NOT NULL AUTO_INCREMENT,
  `FileNo` varchar(20) NOT NULL,
  `Allotmentid` int(5) NOT NULL,
  `PlotInfo` varchar(100) NOT NULL,
  `Owner_MemID` int(5) NOT NULL,
  `Owner_NICN` varchar(20) DEFAULT NULL,
  `Buyer_MemID` int(5) DEFAULT NULL,
  `Buyer_NICN` varchar(20) DEFAULT NULL,
  `FTFees` decimal(20,0) NOT NULL,
  `FTDate` date DEFAULT NULL,
  PRIMARY KEY (`FT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `filetransferhistory` */

/*Table structure for table `memberprofile` */

DROP TABLE IF EXISTS `memberprofile`;

CREATE TABLE `memberprofile` (
  `Memberid` int(5) NOT NULL AUTO_INCREMENT,
  `MembershipNo` varchar(20) NOT NULL,
  `MemberName` varchar(100) NOT NULL,
  `FatherName` varchar(100) DEFAULT NULL,
  `Designation` varchar(50) DEFAULT NULL,
  `Department` varchar(50) DEFAULT NULL,
  `NICN` varchar(20) NOT NULL,
  `DOB` date DEFAULT NULL,
  `NomineeName` varchar(100) DEFAULT NULL,
  `NomineeRelation` varchar(30) DEFAULT NULL,
  `PhoneNo` varchar(20) DEFAULT NULL,
  `CellNo` varchar(20) DEFAULT NULL,
  `Email` varchar(20) DEFAULT NULL,
  `Country` varchar(20) DEFAULT NULL,
  `City` varchar(20) DEFAULT NULL,
  `Street` varchar(40) DEFAULT NULL,
  `HouseNo` varchar(60) DEFAULT NULL,
  `prof_img` text,
  `status` tinyint(4) DEFAULT '0',
  `EntryDate` date DEFAULT NULL,
  PRIMARY KEY (`Memberid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `memberprofile` */

/*Table structure for table `plotscategory` */

DROP TABLE IF EXISTS `plotscategory`;

CREATE TABLE `plotscategory` (
  `PlotID` int(5) NOT NULL AUTO_INCREMENT,
  `PlotType` varchar(20) NOT NULL,
  `PlotCategory` varchar(20) NOT NULL,
  `PlotArea` varchar(20) NOT NULL,
  `PlotDecription` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`PlotID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `plotscategory` */

insert  into `plotscategory`(`PlotID`,`PlotType`,`PlotCategory`,`PlotArea`,`PlotDecription`) values 
(1,'COM','Commercial','100 Sq Yards','For Commercial Purpose'),
(2,'ST','Special Type','1000 Sq Yard','For ST Purpose'),
(3,'B','Residential','400 Sq Yard','For Residential Purpose'),
(4,'A','Residential','200 Sq Yard','For Residential Purpose'),
(5,'R','Residential','120 Sq Yard','For Residential Purpose'),
(6,'AM','Amenity','1000 Sq Yard','For Amenity Purpose');

/*Table structure for table `plotsinventory` */

DROP TABLE IF EXISTS `plotsinventory`;

CREATE TABLE `plotsinventory` (
  `InvID` int(5) NOT NULL AUTO_INCREMENT,
  `PlotNo` varchar(10) NOT NULL,
  `PlotID` int(5) DEFAULT NULL,
  PRIMARY KEY (`InvID`),
  KEY `PlotIdFK` (`PlotID`),
  CONSTRAINT `PlotIdFK` FOREIGN KEY (`PlotID`) REFERENCES `plotscategory` (`PlotID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `plotsinventory` */

/*Table structure for table `receiptsvoucher` */

DROP TABLE IF EXISTS `receiptsvoucher`;

CREATE TABLE `receiptsvoucher` (
  `ReceiptID` int(5) NOT NULL AUTO_INCREMENT,
  `ReceiptDate` date NOT NULL,
  `ReciptNo` varchar(10) NOT NULL,
  `MembershipNo` varchar(20) DEFAULT NULL,
  `ChargesCode` varchar(10) NOT NULL,
  `Descrption` varchar(100) DEFAULT NULL,
  `Amount` decimal(13,3) NOT NULL,
  PRIMARY KEY (`ReceiptID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `receiptsvoucher` */

/*Table structure for table `societyprofile` */

DROP TABLE IF EXISTS `societyprofile`;

CREATE TABLE `societyprofile` (
  `SocietyId` int(10) NOT NULL AUTO_INCREMENT,
  `SocietyFullName` varchar(70) DEFAULT NULL,
  `SocietyShortName` varchar(20) DEFAULT NULL,
  `Town` varchar(50) DEFAULT NULL,
  `SectorNo` varchar(10) DEFAULT NULL,
  `SchemeNo` varchar(10) NOT NULL,
  `AboutSociety` varchar(500) DEFAULT NULL,
  `FoundingDate` date DEFAULT NULL,
  `website` varchar(50) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `contactNo` varchar(12) DEFAULT NULL,
  `logo` varchar(200) NOT NULL,
  PRIMARY KEY (`SocietyId`,`SchemeNo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `societyprofile` */

insert  into `societyprofile`(`SocietyId`,`SocietyFullName`,`SocietyShortName`,`Town`,`SectorNo`,`SchemeNo`,`AboutSociety`,`FoundingDate`,`website`,`address`,`email`,`contactNo`,`logo`) values 
(1,'Karachi University Employees Cooperative Housing ','K.U.E.C.H.S','Gulzar-e-Hijri Karachi','18-A K.D.A','33','Plot no. A-64, KUECHS, scheme 33, sector 18-A, Unnamed Road, Karachi, Karachi University Society Sector 18 A Gulzar E Hijri Scheme 33, Karachi, Karachi City, Sindh','2018-11-16','www.KUECHS.com','PECHS','AF@KUECHS.COM','090099212','NotDefine');

/*Table structure for table `tbl_last_login` */

DROP TABLE IF EXISTS `tbl_last_login`;

CREATE TABLE `tbl_last_login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `sessionData` varchar(2048) NOT NULL,
  `machineIp` varchar(1024) NOT NULL,
  `userAgent` varchar(128) NOT NULL,
  `agentString` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `createdDtm` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_last_login` */

insert  into `tbl_last_login`(`id`,`userId`,`sessionData`,`machineIp`,`userAgent`,`agentString`,`platform`,`createdDtm`) values 
(2,13,'{\"role\":\"1\",\"roleText\":\"EXpress\",\"name\":\"Kamran Iqbal\"}','::1','Chrome 70.0.3538.102','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','Windows 8.1','2018-11-16 18:15:30'),
(3,13,'{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Ahmed Farooq\"}','::1','Chrome 70.0.3538.102','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','Windows 8.1','2018-11-20 11:00:03'),
(4,13,'{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Ahmed Farooq\"}','::1','Chrome 70.0.3538.102','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','Windows 8.1','2018-11-20 11:02:22');

/*Table structure for table `tbl_reset_password` */

DROP TABLE IF EXISTS `tbl_reset_password`;

CREATE TABLE `tbl_reset_password` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `activation_id` varchar(32) NOT NULL,
  `agent` varchar(512) NOT NULL,
  `client_ip` varchar(32) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `createdBy` bigint(20) NOT NULL DEFAULT '1',
  `createdDtm` datetime NOT NULL,
  `updatedBy` bigint(20) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_reset_password` */

insert  into `tbl_reset_password`(`id`,`email`,`activation_id`,`agent`,`client_ip`,`isDeleted`,`createdBy`,`createdDtm`,`updatedBy`,`updatedDtm`) values 
(1,'admin@example.com','hVJbqGLTdn0ryag','Chrome 69.0.3497.100','::1',0,1,'2018-10-02 18:18:56',NULL,NULL);

/*Table structure for table `tbl_roles` */

DROP TABLE IF EXISTS `tbl_roles`;

CREATE TABLE `tbl_roles` (
  `roleId` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT 'role id',
  `role` varchar(50) NOT NULL COMMENT 'role text',
  PRIMARY KEY (`roleId`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_roles` */

insert  into `tbl_roles`(`roleId`,`role`) values 
(1,'Administrator'),
(2,'Manager');

/*Table structure for table `tbl_users` */

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL COMMENT 'login email',
  `password` varchar(128) NOT NULL COMMENT 'hashed login password',
  `name` varchar(128) DEFAULT NULL COMMENT 'full name of user',
  `mobile` varchar(20) DEFAULT NULL,
  `roleId` tinyint(4) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDtm` datetime NOT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_users` */

insert  into `tbl_users`(`userId`,`email`,`password`,`name`,`mobile`,`roleId`,`isDeleted`,`createdBy`,`createdDtm`,`updatedBy`,`updatedDtm`) values 
(13,'afk@hms.com','$2y$10$NrmwkXQhclGMSs0l8gZIAumOnnFxwcpKhCdN5FHvsxp1YXwQjiq82','Ahmed Farooq','0333222221',1,0,1,'2018-10-02 11:53:31',13,'2018-11-16 14:17:07'),
(15,'c2p@hms.com','$2y$10$a/DiXlblQsColAc9PGGZCOaDB2szWITNKQnR.5VobZ3cvs.e0mQNS','Junaid Khan','0333221171',2,1,14,'2018-10-02 18:51:07',13,'2018-11-02 20:17:10'),
(26,'zain@hms.com','$2y$10$NrmwkXQhclGMSs0l8gZIAumOnnFxwcpKhCdN5FHvsxp1YXwQjiq82','Zain','0333222221',1,0,1,'2018-10-02 11:53:31',13,'2018-11-16 14:17:07');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

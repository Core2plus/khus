<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> File Transfer
        <small></small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body table-responsive">
                    <?php $this->load->helper("form"); ?>
                      <form action="<?php echo base_url('main/insert_Society_profile'); ?>" method="post">
                       
                               
                                   <div class="col-md-6">
                                    <div class="form-group">
                                        <label>File No</label>
                                        <input type="text" name="FileNo" class="form-control" required placeholder="File No"/>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Allotment Id</label>
                                        <input type="text"  name="Allotmentid" class="form-control" placeholder="Allotment Id"/>
                                    </div>
                                </div>

                                 

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Owner ID</label>
                                        <input type="text"  name="OwnerID" class="form-control"  placeholder="Owner ID"/>
                                    </div>
                                </div>
                                                                 
                                    

                               <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Owner NICNO:</label>
                                        <input type="text"  name="OwnerNICN" class="form-control"  placeholder="Owner NICNO:"/>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Buyer ID</label>
                                        <input type="text"  name="Buyer_MemID" class="form-control"  placeholder="Buyer ID"/>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Buyer NICNO:</label>
                                        <input type="text"  name="BuyerNICNO" class="form-control" 
                                         placeholder="Buyer NICNO:"/>
                                    </div>
                                </div>

                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>File Transfer Fees</label>
                                        <input type="number"  name="FileTransferFees" class="form-control" 
                                         placeholder="File Transfer Fees"/>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>FTDate</label>
                                        <input type="date"   name="FTDate" class="form-control"  placeholder="FTDate"/>
                                    </div>
                                </div>

                               

                                
                               

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label> Plot Info</label>
                <textarea class="form-control" rows="3" id="comment" name=" Plot Info">  

                </textarea>
                                    </div>
                                </div>


                                <!-- <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Select Status<span class="required">*</span></label>
                                        
                                        <div>
                                            
                                            <select class="form-control" id="sel1" name="sel1">
                                                <option>Select</option>
                                          <option class="form-control" value="1">Enable</option>
                                          <option class="form-control" value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> -->

                                <div class="box-footer col-md-12">
                                    <div class="form-group">
                                        <div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light" style="width: 10%;">
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                </div>
            </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>